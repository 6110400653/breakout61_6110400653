Name: Thanakit Yannam
ID: 6110400653

PANGOORACE'S Breakout...

----------------- For Player -------------------------------------
How to play:
	-in main menu
		-use enter key to accept choice in main menu
		-use key up/down to select menu

	-use arrow key left/right to move paddle
	-you need to move paddle to bounce ball to brick
	-for each level you have 4 hp
	-if ball fall off your's hp will decrease
	-if hp = 0 you died
	-in game have 3 level
	-level 1 is basic gameplay
	-level 2
		-have unbreakable brick (white brick)
		-green brick need to crash many times (when you crash it will become red)
	-level 3 same as level 2 but don't have unbreakable brick
	-you can choose any level in main menu
------------------- good LucK and HF :) --------------------------


----------------- For Developer ---------------------------------
-in main.cpp include 3 level (as function)
-I make RNG function but I didn't use it :(
-in main.cpp also include main menu and level select function
-in level function I can divide to 2 parts
	-1
		-declare variables
		-draw brick in each level
	-2
		-running loop
		-draw UI
		-draw undestory bricks
		-check fall
		-move paddle and check hit
-in each level I code in order of this
-I make class of brick for many kind of brick
	-brick hp
	-brick location
	-brick size
	-unbreakable brick
	-brick can explode (but in game don't have this...)
-in level 1 I use brick old object (struct object)
-but in level 2 and 3 I use my brick class
----------------- I hope this information will help you -----------