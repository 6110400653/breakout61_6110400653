#include <cstdio>
#include <iostream>
#include <cstdlib>
#include <cmath>
#include <ctime>
#include "cp_functions.h"

#define True  1
#define False 0
#define WindowTitle  "PANGOORACE'S Breakout"
#define WindowWidth  800
#define WindowHeight 700

Sound hit_paddle_sound ;
Sound end_sound;
Sound selectSound ;
Sound pressEnter ;
Sound hitSound ;
Sound brickBreak ;
Sound brickDestory ;
Sound fallSound ;
Sound winSound ;
Sound metalHit ;
Texture paddle_texture, ball_texture;
Texture brick_texture, background_texture;
Texture HP_background, HP_bar ;
Texture endChar ;
Font big_font, small_font;
Texture menuBackground ;
Texture menuPoint ;
Texture menuLevel ;
Texture brickHp2 ;
Texture metalBrick ;
Texture gameClear ;
Music bgm ;

enum mainMenu {start, level, exitgame} ;

// Class for storing info for objects, i.e. Paddle, Brick, Ball.
class Object{
public:
    float x, y;
    float width, height;
    float vel_x, vel_y;
    int destroyed;
};

// class brick to make many kind of Bricks in level 2
class Brick{
private:
    float x, y, width, height ;
    int isDestory ;  //  unbreakable brick
    int isBoom ;    // can explode
    int hp ;   // hit many time
public:
    
    Brick(){    // constructor
        this->x = 0 ;
        this->y = 0 ;
    }

    float GetX(){     // accessor
        return x ;
    }
    float GetY(){
        return y ;
    }
    int GetWidth(){
        return width ;
    }
    int GetHeight(){
        return height ;
    }
    int GetHp(){
        return hp ;
    }
    int GetIsboom(){
        return isBoom ;
    }
    int GetIsdestory(){
        return isDestory ;
    }

    void SetHp(int hp){     // mutator 
        this->hp = hp ;
    }
    void SetIsboom(int b){
        isBoom = b ;
    }
    void SetIsdestory(int x){
        isDestory = x ;
    }
    void SetX(float x){
        this->x = x ;
    }
    void SetY(float y){
        this->y = y ;
    } 
    void SetWidth(float w){
        width = w ;
    }
    void SetHeight(float h){
        height = h ;
    }

    //  behavior    
    void DestoryBrick(){
        if (isDestory){
            hp-- ;
        }
    }
    
};

// Collision Detection between two objects a and b
int collide(Object a, Object b)
{
    if (a.x + a.width  < b.x || b.x + b.width  < a.x ||
        a.y + a.height < b.y || b.y + b.height < a.y)
        return False;
    else
        return True;
}
int collideForclass(Object a, Brick b)     // for class brick
{
    if (a.x + a.width  < b.GetX() || b.GetX() + b.GetWidth()  < a.x ||
        a.y + a.height < b.GetY() || b.GetY() + b.GetHeight() < a.y)
        return False;
    else
        return True;
}

// Initial routine to load sounds, textures, and fonts.
int game_init()
{
    hit_paddle_sound = cpLoadSound("hitPaddle.wav");
    end_sound = cpLoadSound("endSound.wav");
    selectSound = cpLoadSound("selectSound.wav") ;
    pressEnter = cpLoadSound("pressEnter.wav") ;
    hitSound = cpLoadSound("hitSound2.wav") ;
    brickBreak = cpLoadSound("brickBreak.wav") ;
    brickDestory = cpLoadSound("brickDestory.wav") ;
    fallSound = cpLoadSound("fallSound.wav") ;
    winSound = cpLoadSound("winSound.wav") ;
    metalHit = cpLoadSound("metalHit.wav") ;

    bgm = cpLoadMusic("bgm.wav") ;

    paddle_texture = cpLoadTexture("paddle2.jpg");
    ball_texture = cpLoadTexture("ball.png");
    brick_texture = cpLoadTexture("brickHp1.png");
    background_texture = cpLoadTexture("backGround2.jpg");
    menuBackground = cpLoadTexture("menuBackground.jpg") ;
    menuPoint = cpLoadTexture("menuPoint.png") ;
    menuLevel = cpLoadTexture("levelSe.jpg") ;
    brickHp2 = cpLoadTexture("brickHp2.png") ;
    endChar = cpLoadTexture("endChar2.png") ;
    metalBrick = cpLoadTexture("metalBrick.png") ;
    gameClear = cpLoadTexture("gameClear.png") ;

    big_font = cpLoadFont("THSarabun.ttf", 60);
    small_font = cpLoadFont("THSarabun.ttf", 30);

    HP_background = cpLoadTexture("HP_background.png") ;
    HP_bar = cpLoadTexture("HP_bar.png") ;

    if (hit_paddle_sound == NULL || 
        hitSound == NULL || end_sound == NULL ||
        paddle_texture == NULL || ball_texture == NULL ||
        brick_texture == NULL || background_texture == NULL ||
        big_font == NULL || small_font == NULL || selectSound == NULL||
        pressEnter == NULL || brickBreak == NULL || brickDestory == NULL ||
        menuBackground == NULL || menuPoint == NULL || menuLevel == NULL ||
        HP_background == NULL || HP_bar == NULL || brickHp2 == NULL ||
        fallSound == NULL || winSound == NULL || endChar == NULL ||
        metalHit == NULL || metalBrick == NULL || gameClear == NULL)
        return False;
    return True;
}

int rng(int min, int max){
    max++ ;
    srand(time(0)) ;
    return min+(rand()%max) ;
}

//  Level 3 
void level3(){
    enum { BALL_VEL_Y = -5, PADDLE_VEL_X = 7 };
    int running, n_hits = 0, score = 0;   
    int hp = 4 ;    // set hp
    char msg[80];
    Object ball = {390, 600, 18, 18, 0, -5, False};
    Object paddle = {WindowWidth/2-64, WindowHeight-50, 128, 5, 0, 0, False};  //128
    Event event;
    cpClearScreen() ;
    cpSwapBuffers();

    

    int nBrick = 64 ;
    Brick brickLevel3[nBrick] ;
    int i, x, y, newX ;
    int index = 0 ;
    x = 284 ;
    y = 100 ;
    i = 1 ;
    int n, lap = 1 ;
    while (i <= 2){
        n = 1 ;
        newX = x ;
        while (n <= lap){
            brickLevel3[index].SetHeight(40) ;
            brickLevel3[index].SetWidth(45) ;
            brickLevel3[index].SetX(newX) ;
            brickLevel3[index].SetY(y) ;
            brickLevel3[index].SetIsdestory(True) ;
            brickLevel3[index].SetIsboom(False) ;
            if (n == 1 || n == lap){
                brickLevel3[index].SetHp(2) ;
            }
            else{
                brickLevel3[index].SetHp(1) ;
            }
            index++ ;
            newX += 45 ;

            n++ ;
        }
        i++ ;
        lap += 2 ;
        y += 40 ;
        x -= 45 ;
    }
    i = 1 ;
    x = 464 ;
    y = 100 ;
    lap = 1 ;
    while (i <= 2){
        n = 1 ;
        newX = x ;
        while (n <= lap){
            brickLevel3[index].SetHeight(40) ;
            brickLevel3[index].SetWidth(45) ;
            brickLevel3[index].SetX(newX) ;
            brickLevel3[index].SetY(y) ;
            brickLevel3[index].SetIsdestory(True) ;
            brickLevel3[index].SetIsboom(False) ;
            if (n == 1 || n == lap){
                brickLevel3[index].SetHp(2) ;
            }
            else{
                brickLevel3[index].SetHp(1) ;
            }
            index++ ;
            newX += 45 ;

            n++ ;
        }
        i++ ;
        lap += 2 ;
        y += 40 ;
        x -= 45 ;
    }
    i = 1 ;
    x = 194 ;
    y = 180 ;
    while (i <= 4){
        n = 1 ;
        newX = x ;
        while (n <= 9){
            brickLevel3[index].SetHeight(40) ;
            brickLevel3[index].SetWidth(45) ;
            brickLevel3[index].SetX(newX) ;
            brickLevel3[index].SetY(y) ;
            brickLevel3[index].SetIsdestory(True) ;
            brickLevel3[index].SetIsboom(False) ;
            if (n == 1 || n == 9){
                brickLevel3[index].SetHp(2) ;
            }
            else{
                brickLevel3[index].SetHp(1) ;
            }
            if (i == 1 && n ==5){
                brickLevel3[index].SetHp(2) ;
            }
            index++ ;
            newX += 45 ;

            n++ ;
        }
        i++ ;
        y += 40 ;
        //x -= 45 ;
    }
    x = 239 ;
    y = 340 ;
    i = 1 ;
    lap = 7 ;
    while (i <= 4){
        n = 1 ;
        newX = x ;
        while (n <= lap){
            brickLevel3[index].SetHeight(40) ;
            brickLevel3[index].SetWidth(45) ;
            brickLevel3[index].SetX(newX) ;
            brickLevel3[index].SetY(y) ;
            brickLevel3[index].SetIsdestory(True) ;
            brickLevel3[index].SetIsboom(False) ;
            if (n == 1 || n == lap){
                brickLevel3[index].SetHp(2) ;
            }
            else{
                brickLevel3[index].SetHp(1) ;
            }
            index++ ;
            newX += 45 ;

            n++ ;
        }


        lap -= 2 ;
        i++ ;
        y += brickLevel3[0].GetHeight() ;
        x += brickLevel3[0].GetWidth() ;
    }
    int notHitPaddle = True ;
    running = True ;
    while (running){
        cpClearScreen();
        cpDrawTexture(255, 255, 255,
            0, 0, WindowWidth, WindowHeight, background_texture);
        cpDrawTexture(255, 255, 255,
            paddle.x, paddle.y, paddle.width, paddle.height, paddle_texture);
        cpDrawTexture(255, 255, 255,
            ball.x, ball.y, ball.width, ball.height, ball_texture);
        cpDrawTexture(255, 255, 255, 527, 21, 251, 37, HP_background) ;   // make hp bar
        cpDrawTexture(255, 255, 255, 527, 21, 251*hp/4, 37, HP_bar) ;
        sprintf(msg, "HP:");                                           
        cpDrawText(255, 255, 255, 460, 5, msg, big_font, 0);
        sprintf(msg, "SCORE: %d", score);                                           
        cpDrawText(255, 255, 255, 7, 5, msg, big_font, 0);
        //sprintf(msg, "x: %.2f  y: %.2f hit:%d    vel_x: %.2f  vel_y: %.2f   paddle: %.2f", ball.x, ball.y, notHitPaddle, ball.vel_x, ball.vel_y, paddle.x);                                            //  top text
        //cpDrawText(255, 255, 255, 3, 3, msg, small_font, 0);

        for (int i=0; i<nBrick; i++){
            if (brickLevel3[i].GetHp()){
                if (brickLevel3[i].GetHp() == 1){
                    cpDrawTexture(255, 255, 255, brickLevel3[i].GetX(), brickLevel3[i].GetY()
                        , brickLevel3[i].GetWidth(), brickLevel3[i].GetHeight(), brick_texture) ;
                }
                else if (brickLevel3[i].GetHp() == 2){
                    cpDrawTexture(255, 255, 255, brickLevel3[i].GetX(), brickLevel3[i].GetY()
                        , brickLevel3[i].GetWidth(), brickLevel3[i].GetHeight(), brickHp2) ;
                }
            }
    
        }
        cpSwapBuffers() ;

        if (ball.y + ball.width > WindowHeight || n_hits == nBrick+18) {
            if (ball.y + ball.width > WindowHeight){
                hp-- ;
            }
            else if (n_hits == nBrick+18){
                cpPlaySound(winSound) ;
                cpDrawTexture(255, 255, 255, 224, 276, 350, 140, gameClear) ;
                cpSwapBuffers();
                cpDelay(3000) ;
                while (1) {
                    cbEventListener(&event);
                    
                    if (event.type == QUIT ||
                        event.type == KEYUP && event.key.keysym.sym == K_ESCAPE) {
                        running = False;
                        cpCleanUp();
                        exit(0) ;
                    }
                }
            }
            if (hp == 0){
                cpPlaySound(end_sound);
                cpDrawTexture(255, 255, 255, 224, 276, 350, 140, endChar) ;
                cpDrawTexture(255, 255, 255, 527, 21, 251, 37, HP_background) ;
                cpDrawTexture(255, 255, 255, 527, 21, 0, 37, HP_bar) ;
                cpSwapBuffers();
                while (1) {
                    cbEventListener(&event);
                    if (event.type == QUIT ||
                        event.type == KEYUP && event.key.keysym.sym == K_ESCAPE) {
                        running = False;
                        cpCleanUp();
                        exit(0) ;
                    }
                }
            }
            else{
                cpPlaySound(fallSound) ;
                cpDelay(1000) ;
                ball.x = 390 ;
                ball.y = 600 ;
                ball.vel_x = 3 ;
                ball.vel_y = -5 ;
                cpDrawTexture(255, 255, 255,
                    paddle.x, paddle.y, paddle.width, paddle.height, paddle_texture);
                cpDrawTexture(255, 255, 255,
                    ball.x, ball.y, ball.width, ball.height, ball_texture);
                sprintf(msg, "x: %.2f  y: %.2f hit:%d    vel_x: %.2f  vel_y: %.2f   paddle: %.2f", ball.x, ball.y, notHitPaddle, ball.vel_x, ball.vel_y, paddle.x);                                            //  top text
                cpDrawText(255, 255, 255, 3, 3, msg, small_font, 0);
                cpSwapBuffers();
            }
        }
        

        while (cbEventListener(&event)) {
            if (event.type == QUIT ||
                 event.type == KEYUP && event.key.keysym.sym == K_ESCAPE) {
                running = False;
                cpCleanUp();
                exit(0) ;
            }

            if (event.type == KEYDOWN) {
                if (event.key.keysym.sym == K_LEFT)
                    paddle.vel_x = -abs(PADDLE_VEL_X);
                if (event.key.keysym.sym == K_RIGHT)
                    paddle.vel_x = abs(PADDLE_VEL_X);
            }
            else
            if (event.type == KEYUP) {
                if (event.key.keysym.sym == K_LEFT)
                    paddle.vel_x = 0;
                if (event.key.keysym.sym == K_RIGHT)
                    paddle.vel_x = 0;
            }
        }
        paddle.x += paddle.vel_x;

        if (paddle.x < 0)
            paddle.x = 0;
        if (paddle.x + paddle.width > WindowWidth)
            paddle.x = WindowWidth - paddle.width;

        ball.x += ball.vel_x;
        ball.y += ball.vel_y;

        if (ball.x < 0 || ball.x + ball.width > WindowWidth){     // hit side
            cpPlaySound(hitSound); 
            if (ball.x < 0){
                ball.x = 1 ;
            }
            else if (ball.x + ball.width > WindowWidth){
                ball.x = WindowWidth-ball.width-1 ;
            }
            ball.vel_x = -ball.vel_x;
            notHitPaddle = 3 ;
        }

        if (ball.y <= 80) {
            cpPlaySound(hitSound);       //hit top
            ball.vel_y = -ball.vel_y;
            notHitPaddle = 2 ;
        }

        for (int n = 0; n < nBrick; n++) {              // hit brick
            if (brickLevel3[n].GetHp() &&
                collideForclass(ball, brickLevel3[n]) == True) {
                brickLevel3[n].DestoryBrick() ;
                if (brickLevel3[n].GetHp() == 0){
                    cpPlaySound(brickDestory);
                }
                else if (brickLevel3[n].GetHp() != 0){
                    cpPlaySound(brickBreak);
                }

                if (ball.x+1 >= brickLevel3[n].GetX()+brickLevel3[n].GetWidth() || ball.x+(ball.width/2) <= brickLevel3[n].GetX()){
                    ball.vel_y = ball.vel_y;
                }
                else {
                    ball.vel_y = -ball.vel_y;
                }

                float tempVel ;
                tempVel = (ball.x+ball.width/2) - (brickLevel3[n].GetX()+brickLevel3[n].GetWidth()/2) ;
                tempVel = tempVel/10 ;
                ball.vel_x = tempVel ;
                n_hits++;
                score += 10;
                notHitPaddle = True ;
                break;
            }
        }

        if (collide(ball, paddle) == True && notHitPaddle) {   // hit paddle
            notHitPaddle = False ;
            cpPlaySound(hit_paddle_sound);
            ball.vel_y = -ball.vel_y;

            float tempVel ;
            tempVel = (ball.x+ball.width/2) - (paddle.x+paddle.width/2) ;
            tempVel = tempVel/10 ;
            ball.vel_x = tempVel ;
        }

        cpDelay(10);


    }
    cpCleanUp();

}


//  Level 2 
void level2(){
    enum { BALL_VEL_Y = -5, PADDLE_VEL_X = 7 };
    int running, n_hits = 0, score = 0;    
    int hp = 4 ;    // set hp
    char msg[80];
    Object ball = {390, 600, 18, 18, 0, -5, False};
    Object paddle = {WindowWidth/2-64, WindowHeight-50, 128, 5, 0, 0, False};
    Event event;
    cpClearScreen() ;
    cpSwapBuffers();

    
    int nBrick = 41 ;
    Brick brickLevel2[nBrick] ;
    int i, x, y, newX ;
    int index = 0 ;
    x = 375 ;
    y = 97 ;
    i = 1 ;
    int n, lap = 1 ;
    while (i <= 5){
        n = 1 ;
        newX = x ;
        while (n <= lap){
            brickLevel2[index].SetHeight(41) ;
            brickLevel2[index].SetWidth(67) ;
            brickLevel2[index].SetX(newX) ;
            brickLevel2[index].SetY(y) ;
            brickLevel2[index].SetIsdestory(True) ;
            brickLevel2[index].SetIsboom(False) ;
            if (i == 5 && (n == 1 || n == lap)){
                brickLevel2[index].SetIsdestory(False) ;
                brickLevel2[index].SetHp(1) ;
            }
            else if (n == 1 || n == lap){
                brickLevel2[index].SetHp(2) ;
            }
            else{
                brickLevel2[index].SetHp(1) ;
            }
            index++ ;
            newX += 67 ;

            n++ ;
        }
        i++ ;
        lap += 2 ;
        y += 41 ;
        x -= 67 ;
    }
    x = 174 ;
    i = 1 ;
    lap = 7 ;
    while (i <= 4){
        n = 1 ;
        newX = x ;
        while (n <= lap){
            brickLevel2[index].SetHeight(41) ;
            brickLevel2[index].SetWidth(67) ;
            brickLevel2[index].SetX(newX) ;
            brickLevel2[index].SetY(y) ;
            brickLevel2[index].SetIsdestory(True) ;
            brickLevel2[index].SetIsboom(False) ;
            if (n == 1 || n == lap){
                brickLevel2[index].SetHp(2) ;
            }
            else{
                brickLevel2[index].SetHp(1) ;
            }
            index++ ;
            newX += 67 ;

            n++ ;
        }


        lap -= 2 ;
        i++ ;
        y += brickLevel2[0].GetHeight() ;
        x += brickLevel2[0].GetWidth() ;
    }
    int notHitPaddle = True ;
    running = True ;
    while (running){
        cpClearScreen();
        cpDrawTexture(255, 255, 255,
            0, 0, WindowWidth, WindowHeight, background_texture);
        cpDrawTexture(255, 255, 255,
            paddle.x, paddle.y, paddle.width, paddle.height, paddle_texture);
        cpDrawTexture(255, 255, 255,
            ball.x, ball.y, ball.width, ball.height, ball_texture);
        cpDrawTexture(255, 255, 255, 527, 21, 251, 37, HP_background) ;   // make hp bar
        cpDrawTexture(255, 255, 255, 527, 21, 251*hp/4, 37, HP_bar) ;
        sprintf(msg, "HP:");                                           
        cpDrawText(255, 255, 255, 460, 5, msg, big_font, 0);
        sprintf(msg, "SCORE: %d", score);                                           
        cpDrawText(255, 255, 255, 7, 5, msg, big_font, 0);
        //sprintf(msg, "x: %.2f  y: %.2f hit:%d    vel_x: %.2f  vel_y: %.2f   paddle: %.2f", ball.x, ball.y, notHitPaddle, ball.vel_x, ball.vel_y, paddle.x);                                            //  top text
        //cpDrawText(255, 255, 255, 3, 3, msg, small_font, 0);

        for (int i=0; i<nBrick; i++){
            if (brickLevel2[i].GetHp()){
                if (brickLevel2[i].GetIsdestory() == 0){
                    cpDrawTexture(255, 255, 255, brickLevel2[i].GetX(), brickLevel2[i].GetY()
                        , brickLevel2[i].GetWidth(), brickLevel2[i].GetHeight(), metalBrick) ;
                }
                else if (brickLevel2[i].GetHp() == 1){
                    cpDrawTexture(255, 255, 255, brickLevel2[i].GetX(), brickLevel2[i].GetY()
                        , brickLevel2[i].GetWidth(), brickLevel2[i].GetHeight(), brick_texture) ;
                }
                else if (brickLevel2[i].GetHp() == 2){
                    cpDrawTexture(255, 255, 255, brickLevel2[i].GetX(), brickLevel2[i].GetY()
                        , brickLevel2[i].GetWidth(), brickLevel2[i].GetHeight(), brickHp2) ;
                }
            }
            
    
        }
        cpSwapBuffers() ;

        if (ball.y + ball.width > WindowHeight || n_hits == nBrick+12) {
            if (ball.y + ball.width > WindowHeight){
                hp-- ;
            }
            else if (n_hits == nBrick+12){
                cpPlaySound(winSound) ;
                cpDelay(3000) ;
                level3() ;   // go to next level 
                while (1) {
                    cbEventListener(&event);
                    if (event.type == QUIT ||
                        event.type == KEYUP && event.key.keysym.sym == K_ESCAPE) {
                        cpCleanUp();
                        exit(0) ;
                    }
                }
            }
            if (hp == 0){
                cpPlaySound(end_sound);
                cpDrawTexture(255, 255, 255, 224, 276, 350, 140, endChar) ;
                cpDrawTexture(255, 255, 255, 527, 21, 251, 37, HP_background) ;
                cpDrawTexture(255, 255, 255, 527, 21, 0, 37, HP_bar) ;
                cpSwapBuffers();
                while (1) {
                    cbEventListener(&event);
                    if (event.type == QUIT ||
                        event.type == KEYUP && event.key.keysym.sym == K_ESCAPE) {
                        cpCleanUp();
                        exit(0) ;
                    }
                }
            }
            else{
                cpPlaySound(fallSound) ;
                cpDelay(1000) ;
                ball.x = 390 ;
                ball.y = 600 ;
                ball.vel_x = 3 ;
                ball.vel_y = -5 ;
                cpDrawTexture(255, 255, 255,
                    paddle.x, paddle.y, paddle.width, paddle.height, paddle_texture);
                cpDrawTexture(255, 255, 255,
                    ball.x, ball.y, ball.width, ball.height, ball_texture);
                sprintf(msg, "x: %.2f  y: %.2f hit:%d    vel_x: %.2f  vel_y: %.2f   paddle: %.2f", ball.x, ball.y, notHitPaddle, ball.vel_x, ball.vel_y, paddle.x);                                            //  top text
                cpDrawText(255, 255, 255, 3, 3, msg, small_font, 0);
                cpSwapBuffers();
            }
        }
        

        while (cbEventListener(&event)) {
            if (event.type == QUIT ||
                 event.type == KEYUP && event.key.keysym.sym == K_ESCAPE) {
                running = False;
                cpCleanUp();
                exit(0) ;
            }

            if (event.type == KEYDOWN) {
                if (event.key.keysym.sym == K_LEFT)
                    paddle.vel_x = -abs(PADDLE_VEL_X);
                if (event.key.keysym.sym == K_RIGHT)
                    paddle.vel_x = abs(PADDLE_VEL_X);
            }
            else
            if (event.type == KEYUP) {
                if (event.key.keysym.sym == K_LEFT)
                    paddle.vel_x = 0;
                if (event.key.keysym.sym == K_RIGHT)
                    paddle.vel_x = 0;
            }
        }
        paddle.x += paddle.vel_x;

        if (paddle.x < 0)
            paddle.x = 0;
        if (paddle.x + paddle.width > WindowWidth)
            paddle.x = WindowWidth - paddle.width;

        ball.x += ball.vel_x;
        ball.y += ball.vel_y;

        if (ball.x <= 0 || ball.x + ball.width > WindowWidth){     // hit side
            cpPlaySound(hitSound); 
            if (ball.x < 0){
                ball.x = 1 ;
            }
            else if (ball.x + ball.width > WindowWidth){
                ball.x = WindowWidth-ball.width-1 ;
            }
            ball.vel_x = -ball.vel_x;
            notHitPaddle = 3 ;
        }

        if (ball.y <= 80) {
            cpPlaySound(hitSound);      //hit top
            ball.vel_y = -ball.vel_y;
            notHitPaddle = 2 ;
        }

        for (int n = 0; n < nBrick; n++) {              // hit brick
            if (brickLevel2[n].GetHp() &&
                collideForclass(ball, brickLevel2[n]) == True) {
                brickLevel2[n].DestoryBrick() ;
                if (brickLevel2[n].GetIsdestory() == 0){
                    cpPlaySound(metalHit) ;
                }
                else if (brickLevel2[n].GetHp() == 0){
                    cpPlaySound(brickDestory);
                    n_hits++;
                    score += 10;
                }
                else if (brickLevel2[n].GetHp() != 0){
                    cpPlaySound(brickBreak);
                    n_hits++;
                }

                if (ball.x+1 >= brickLevel2[n].GetX()+brickLevel2[n].GetWidth() || ball.x+(ball.width/2) <= brickLevel2[n].GetX()){
                    ball.vel_y = ball.vel_y;
                }
                else {
                    ball.vel_y = -ball.vel_y;
                }
                
                float tempVel ;
                tempVel = (ball.x+ball.width/2) - (brickLevel2[n].GetX()+brickLevel2[n].GetWidth()/2) ;
                tempVel = tempVel/10 ;
                ball.vel_x = tempVel ;
                
                notHitPaddle = True ;
                break;
            }
        }

        if (collide(ball, paddle) == True && notHitPaddle) {   // hit paddle
            notHitPaddle = False ;
            cpPlaySound(hit_paddle_sound);
            ball.vel_y = -ball.vel_y;

            float tempVel ;
            tempVel = (ball.x+ball.width/2) - (paddle.x+paddle.width/2) ;
            tempVel = tempVel/10 ;
            ball.vel_x = tempVel ;
        }

        cpDelay(10);


    }

}

// level 1
void level1(){
    enum { BALL_VEL_Y = -5, PADDLE_VEL_X = 7 };
    int running, n_bricks = 10*8, n_hits = 0, score = 0;    // 1 row = 15 bricks
    int hp = 4 ;    // set hp
    char msg[80];
    Object bricks[n_bricks];
    Object ball = {390, 600, 18, 18, 0, -5, False};
    Object paddle = {WindowWidth/2-64, WindowHeight-50, 128, 5, 0, 0, False};
    Event event;

    for (int n = 0, x = 52, y = 109; n < n_bricks; n++) {    // brick 8 row 
        if (n%10 == 0 && n){
            y += 34 ;
            x = 52 ;
        }
        bricks[n].width = 70;
        bricks[n].height = 34;
        bricks[n].x = x;
        bricks[n].y = y;
        bricks[n].destroyed = False;
        x += bricks[n].width;
    }

    running = True;
    int notHitPaddle = True ;   //  debug ball stuck in paddle 
    while (running) {


        cpClearScreen();
        cpDrawTexture(255, 255, 255,
            0, 0, WindowWidth, WindowHeight, background_texture);
        cpDrawTexture(255, 255, 255,
            paddle.x, paddle.y, paddle.width, paddle.height, paddle_texture);
        cpDrawTexture(255, 255, 255,
            ball.x, ball.y, ball.width, ball.height, ball_texture);
        cpDrawTexture(255, 255, 255, 527, 21, 251, 37, HP_background) ;   // make hp bar
        cpDrawTexture(255, 255, 255, 527, 21, 251*hp/4, 37, HP_bar) ;
        sprintf(msg, "HP:");                                           
        cpDrawText(255, 255, 255, 460, 5, msg, big_font, 0);
        sprintf(msg, "SCORE: %d", score);                                           
        cpDrawText(255, 255, 255, 7, 5, msg, big_font, 0);
        //sprintf(msg, "x: %.2f  y: %.2f hit:%d    vel_x: %.2f  vel_y: %.2f   paddle: %.2f", ball.x, ball.y, notHitPaddle, ball.vel_x, ball.vel_y, paddle.x);                                            //  top text
        //cpDrawText(255, 255, 255, 3, 3, msg, small_font, 0);

        for (int n = 0; n < n_bricks; n++) {
            if (!bricks[n].destroyed){
                cpDrawTexture(255, 255, 255,
                    bricks[n].x, bricks[n].y, bricks[n].width, bricks[n].height,
                    brick_texture);
            }
        }


        if (ball.y + ball.width > WindowHeight || n_hits == n_bricks) {
            if (ball.y + ball.width > WindowHeight){
                hp-- ;
            }
            else if (n_hits == n_bricks){
                cpPlaySound(winSound) ;
                cpDelay(3000) ;
                level2() ;   // go to next level 
                while (1) {
                    cbEventListener(&event);
                    if (event.type == QUIT ||
                        event.type == KEYUP && event.key.keysym.sym == K_ESCAPE) {
                        cpCleanUp();
                        exit(0) ;
                    }
                }
            }
            if (hp == 0){
                cpPlaySound(end_sound);
                cpDrawTexture(255, 255, 255, 224, 276, 350, 140, endChar) ;
                cpDrawTexture(255, 255, 255, 527, 21, 251, 37, HP_background) ;
                cpDrawTexture(255, 255, 255, 527, 21, 0, 37, HP_bar) ;
                cpSwapBuffers();
                while (1) {
                    cbEventListener(&event);
                    if (event.type == QUIT ||
                        event.type == KEYUP && event.key.keysym.sym == K_ESCAPE) {
                        cpCleanUp();
                        exit(0) ;
                    }
                }
            }
            else{
                cpPlaySound(fallSound) ;
                cpDelay(1000) ;
                ball.x = WindowWidth/2 ;
                ball.y = 450 ;
                ball.vel_x = 3 ;
                ball.vel_y = -5 ;
                cpDrawTexture(255, 255, 255,
                    paddle.x, paddle.y, paddle.width, paddle.height, paddle_texture);
                cpDrawTexture(255, 255, 255,
                    ball.x, ball.y, ball.width, ball.height, ball_texture);
                sprintf(msg, "x: %.2f  y: %.2f hit:%d    vel_x: %.2f  vel_y: %.2f   paddle: %.2f", ball.x, ball.y, notHitPaddle, ball.vel_x, ball.vel_y, paddle.x);                                            //  top text
                cpDrawText(255, 255, 255, 3, 3, msg, small_font, 0);
                cpSwapBuffers();
            }
        }
        
        cpSwapBuffers();

        while (cbEventListener(&event)) {
            if (event.type == QUIT ||
                 event.type == KEYUP && event.key.keysym.sym == K_ESCAPE) {
                running = False;
                break;
            }

            if (event.type == KEYDOWN) {
                if (event.key.keysym.sym == K_LEFT)
                    paddle.vel_x = -abs(PADDLE_VEL_X);
                if (event.key.keysym.sym == K_RIGHT)
                    paddle.vel_x = abs(PADDLE_VEL_X);
            }
            else
            if (event.type == KEYUP) {
                if (event.key.keysym.sym == K_LEFT)
                    paddle.vel_x = 0;
                if (event.key.keysym.sym == K_RIGHT)
                    paddle.vel_x = 0;
            }
        }
        paddle.x += paddle.vel_x;

        if (paddle.x < 0)
            paddle.x = 0;
        if (paddle.x + paddle.width > WindowWidth)
            paddle.x = WindowWidth - paddle.width;

        ball.x += ball.vel_x;
        ball.y += ball.vel_y;

        if (ball.x <= 0 || ball.x + ball.width > WindowWidth){      // hit side
            cpPlaySound(hitSound); 
            if (ball.x < 0){
                ball.x = 1 ;
            }
            else if (ball.x + ball.width > WindowWidth){
                ball.x = WindowWidth-ball.width-1 ;
            }
            ball.vel_x = -ball.vel_x;
            notHitPaddle = 3 ;
        }

        if (ball.y <= 80) {
            cpPlaySound(hitSound);      //hit top
            ball.vel_y = -ball.vel_y;
            notHitPaddle = 2 ;
        }

        for (int n = 0; n < n_bricks; n++) {              // hit brick
            if (!bricks[n].destroyed &&
                collide(ball, bricks[n]) == True) {
                cpPlaySound(brickDestory);
                if (ball.x+2 >= bricks[n].x+bricks[n].width || ball.x+(ball.width/2) <= bricks[n].x){
                    ball.vel_y = ball.vel_y;
                }
                else {
                    ball.vel_y = -ball.vel_y;
                }

                float tempVel ;
                tempVel = (ball.x+ball.width/2) - (bricks[n].x+bricks[n].width/2) ;
                tempVel = tempVel/10 ;
                ball.vel_x = tempVel ;

                bricks[n].destroyed = True;
                n_hits++;
                score += 10;
                notHitPaddle = True ;
                break;
            }
        }

        if (collide(ball, paddle) == True && notHitPaddle) {   // hit paddle
            notHitPaddle = False ;
            cpPlaySound(hit_paddle_sound);
            ball.vel_y = -ball.vel_y;

            float tempVel ;
            tempVel = (ball.x+ball.width/2) - (paddle.x+paddle.width/2) ;
            tempVel = tempVel/10 ;
            ball.vel_x = tempVel ;
        }

        cpDelay(10);
    }

}

// level select menu
void levelSelect(){
    Event event ;
    int inMenu = True ;
    int choice = 0 ;

    while (inMenu){
        cpClearScreen();
        cpDrawTexture(255, 255, 255,
            0, 0, WindowWidth, WindowHeight, menuLevel);
        if (choice == 0){
            cpDrawTexture(255, 255, 255,
                190, 210, 75, 80, menuPoint);
            
        }
        else if (choice == 1){
            cpDrawTexture(255, 255, 255,
                190, 323, 75, 80, menuPoint) ;
        }
        else if (choice == 2){
            cpDrawTexture(255, 255, 255,
                190, 446, 75, 80, menuPoint);
        }
        else if (choice == 3){
            cpDrawTexture(255, 255, 255,
                530, 600, 75, 80, menuPoint);
        }
        
        while (cbEventListener(&event)) {     // press enter
            if (event.type == KEYUP && event.key.keysym.sym == K_ENTER){
                cpPlaySound(pressEnter) ;
                cpDelay(1000) ;
                if (choice == 0){
                    level1() ;
                    cpCleanUp();
                    exit(0) ;
                }
                else if (choice == 1){
                    level2() ;
                    cpCleanUp();
                    exit(0) ;
                }
                else if (choice == 2){
                    level3() ;
                    cpCleanUp();
                    exit(0) ;
                }
                else if (choice == 3){
                    inMenu = False ;
                    break ;
                }
            }
            if (event.type == QUIT ||
                 event.type == KEYUP && event.key.keysym.sym == K_ESCAPE) {
                cpCleanUp();
                exit(0) ;
            }

            if (event.type == KEYDOWN) {
                if (event.key.keysym.sym == K_DOWN){
                    cpPlaySound(selectSound) ;
                    choice++ ;
                }
                if (event.key.keysym.sym == K_UP){
                    cpPlaySound(selectSound) ;
                    choice-- ;
                }
                if (choice > 3)
                    choice = 0 ;
                if (choice < 0)
                    choice = 3 ;
            }
            else
            if (event.type == KEYUP) {
                if (event.key.keysym.sym == K_DOWN)
                    choice += 0 ;
                if (event.key.keysym.sym == K_UP)
                    choice += 0 ;
            }
        }
        cpSwapBuffers();
        cpDelay(10);
    }

}

// main menu
void inMainMenu(){
    Event event ;
    int inMenu = True ;
    int choice = 0 ;

    while (inMenu){
        cpClearScreen();
        cpDrawTexture(255, 255, 255,
            0, 0, WindowWidth, WindowHeight, menuBackground);
        if (choice == 0){
            cpDrawTexture(255, 255, 255,
                190, 310, 75, 80, menuPoint);
            
        }
        else if (choice == 1){
            cpDrawTexture(255, 255, 255,
                190, 415, 75, 80, menuPoint) ;
        }
        else if (choice == 2){
            cpDrawTexture(255, 255, 255,
                190, 520, 75, 80, menuPoint);
        }
        
        while (cbEventListener(&event)) {     // press Enter
            if (event.type == KEYUP && event.key.keysym.sym == K_ENTER){
                cpPlaySound(pressEnter) ;
                cpDelay(1000) ;
                if (choice == 0){
                    level1() ;
                    cpCleanUp();
                    exit(0) ;
                }
                else if (choice == 1){
                    levelSelect() ;
                }
                else if (choice == 2){
                    cpCleanUp();
                    exit(0) ;
                }
            }
            if (event.type == QUIT ||
                 event.type == KEYUP && event.key.keysym.sym == K_ESCAPE) {
                cpCleanUp();
                exit(0) ;
            }

            if (event.type == KEYDOWN) {
                if (event.key.keysym.sym == K_DOWN){
                    cpPlaySound(selectSound) ;
                    choice++ ;
                }
                if (event.key.keysym.sym == K_UP){
                    cpPlaySound(selectSound) ;
                    choice-- ;
                }
                if (choice > 2)
                    choice = 0 ;
                if (choice < 0)
                    choice = 2 ;
            }
            else
            if (event.type == KEYUP) {
                if (event.key.keysym.sym == K_DOWN)
                    choice += 0 ;
                if (event.key.keysym.sym == K_UP)
                    choice += 0 ;
            }
        }
        cpSwapBuffers();
        cpDelay(10);
    }

}



int main(int argc, char *args[])
{
    enum { BALL_VEL_Y = -5, PADDLE_VEL_X = 7 };
    int running, n_bricks = 15*8, n_hits = 0, score = 0;    // 1 row = 15 bricks
    int hp = 4 ;    // set hp
    char msg[80];
    Object bricks[n_bricks];
    Object ball = {WindowWidth/2, 300, 18, 18, 0, -5, False};
    Object paddle = {WindowWidth/2-64, WindowHeight-50, 128, 18, 0, 0, False};
    Event event;

    if (cpInit(WindowTitle, WindowWidth, WindowHeight) == False) {
        fprintf(stderr, "Window initialization failed!\n");
        exit(1);
    }

    if (game_init() == False) {
        fprintf(stderr, "Game initialization failed!\n");
        exit(1);
    }

    
    cpPlayMusic(bgm) ;
    inMainMenu() ;

    
    cpCleanUp();
    return 0;
}
